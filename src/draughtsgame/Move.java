package draughtsgame;

import java.awt.*;

/**
 * Simply structure with decision points
 */
public class Move {
    public Point from;
    public Point to;

    public Move(Point from, Point to) {
        this.from = from;
        this.to = to;
    }
}
