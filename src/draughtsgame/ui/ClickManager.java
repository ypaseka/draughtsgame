/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draughtsgame.ui;

import java.awt.*;

/**
 * This class is to manage clicks.
 * It can recognise a move from two clicks
 */
class ClickManager {
    Point lastClicked;
    UserInterface uiInterface;


    ClickManager(UserInterface uiInterface) {
        this.uiInterface = uiInterface;

        lastClicked = null;
    }

    void handle(Point clickedPoint) {
        // If two different points were clicked, create move request
        lastClicked = clickedPoint;
        System.out.println("Clicked pawn");
    }

    void ClickedEmptySpace(Point clickedSpace) {
        System.out.println("Clicked empty");
        if (lastClicked == null)
            return;

        // If two different points were clicked, create move request
        uiInterface.MoveDraughtsmanRequest(lastClicked, clickedSpace);
        ClickClear();
    }

    void ClickClear() {
        lastClicked = null;
    }
}
