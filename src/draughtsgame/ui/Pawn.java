/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draughtsgame.ui;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.awt.*;

class Pawn {
    Circle[] shapes;
    public Point point;

    void SetPosition(Point point) {
        if (this.point != null)
            System.out.println("Point " + this.point.toString() + " " + point.toString());
        this.point = new Point(point);

        for (Circle shape : shapes) {
            shape.setCenterX((point.x + 0.5) * Board.boardFieldSize);
            shape.setCenterY((point.y + 0.5) * Board.boardFieldSize);
        }
    }

    boolean display = false;

    Pawn(Point point, boolean white, Group root) {
        this.shapes = new Circle[]{
                new Circle(0, 0, Board.boardFieldSize / 2),
                new Circle(0, 0, Board.boardFieldSize * 0.48)
        };

        shapes[0].setMouseTransparent(true);
        shapes[1].setMouseTransparent(true);


        shapes[0].setTranslateX(50);
        shapes[0].setTranslateY(50);
        shapes[1].setTranslateX(50);
        shapes[1].setTranslateY(50);

        SetPosition(point);

        if (white)
            shapes[1].setFill(Color.WHITE);
        else
            shapes[1].setFill(Color.BLACK);

        shapes[0].setFill(Color.WHITE);
    }
}
