/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draughtsgame.ui;

import javafx.scene.Group;
import javafx.scene.paint.Color;

import java.awt.*;

public class Dame extends Pawn {

    public Dame(Point point, boolean white, Group root) {
        super(point, white, root);

        shapes[0].setFill(Color.RED);
    }

}
