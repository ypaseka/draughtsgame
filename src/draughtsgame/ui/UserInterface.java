/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draughtsgame.ui;

import draughtsgame.Game;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;

/**
 * This class is responsible only for displaying game board and all necessary
 * UI elements.
 */

public class UserInterface extends Application {
    private final Game game;
    private final Board board;
    private final Menu menu;
    
    public UserInterface(){
        game = Game.instance;
        clickManager = new ClickManager(this);
        
        this.board = new Board(this);
        this.menu = new Menu(this);
        game.ui = this;
    }
    
    ClickManager clickManager;

    public void changeInUI() {
        System.out.println("Changing the UI");
    }
    
    public static void launchUI(String[] args){
        launch(args);
    }
    
    public void CreateDraughtsman(Point point, boolean white)
    {
        board.AddDraught(point, white);
    }
    
    public void CreateDame(Point point, boolean white)
    {
        board.AddDame(point, white);
    }
    
    public void MoveDraughtsman(Point from, Point to)
    {
        board.MoveDraught(from, to);
        clickManager.ClickClear();
    }
    
    public void RemoveDraughtsman(Point position)
    {
        board.RemoveDraught(position);
    }
    
    void MoveDraughtsmanRequest(Point from, Point to)
    {
        game.MoveRequest(from, to);
    }

    @Override
    public void start(Stage primaryStage) {

        game.UIInited();
        
        primaryStage.setTitle("Draughts game");
        
        Group root = new Group();
        board.setPane(root);
        menu.setPane(root);
        root.getChildren().add(board);
        root.getChildren().add(menu);
        primaryStage.setScene(new Scene(root, 850, 700));
        primaryStage.show();
    }
}
