/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draughtsgame.ui;

import ai.AI;
import draughtsgame.Game;
import draughtsgame.LogicalBoard;
import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.awt.*;


public class Menu extends Canvas {
    GraphicsContext gc;
    private Group root;
    private UserInterface uiInterface;
    private Text opCount = null;
    private Text opDCount = null;
    private Text myCount = null;
    private Text myDCount = null;
    private Text sDeep = null;
    private Text sBeta = null;
    public static Menu menu;

    public Menu(UserInterface uiInterface) {
        super(600, 600);
        menu = this;
        this.setMouseTransparent(true);
        this.uiInterface = uiInterface;

        this.gc = getGraphicsContext2D();

        final AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                if (root == null)
                    return;
                Game game = Game.instance;
                if (game == null)
                    return;
                opCount.setText(Integer.toString(game.GetDraughtmansCount(LogicalBoard.fieldState.BLACK_DRAUGHT)));
                myCount.setText(Integer.toString(game.GetDraughtmansCount(LogicalBoard.fieldState.WHITE_DRAUGHT)));

                opDCount.setText(Integer.toString(game.GetDraughtmansCount(LogicalBoard.fieldState.BLACK_DAME)));
                myDCount.setText(Integer.toString(game.GetDraughtmansCount(LogicalBoard.fieldState.WHITE_DAME)));

                sDeep.setText(Integer.toString(AI.searchDeep));
                sBeta.setText(Double.toString(AI.lastBeta));


            }

        };

        timer.start();

    }

    public void DrawMenu() {
        gc.setFill(Color.BLACK);
        gc.fillRect(10, 10,
                10, 10);
    }

    Text AddMenuItem(String text, Point point, int fontSize) {
        // Rectangle rect = new Rectangle(35, 19 + i * 50, 180, 40);
        // rect.setFill(Color.GREEN);
        Text _startGame = new Text(point.x, point.y + fontSize, text);
        _startGame.setFont(new Font(fontSize));
        _startGame.setFill(Color.BLACK);

        root.getChildren().add(_startGame);

        return _startGame;
    }

    public void wonPlane(boolean you) {
        // Add blocks
        Rectangle rect = new Rectangle(0, 0, 1000, 1000);
        rect.setFill(Color.WHITE);
        root.getChildren().add(rect);


        AddMenuItem((you ? "You" : "Computer") + " won", new Point(300, 300), 40);
    }

    void setPane(Group root) {
        this.root = root;

        // Add all menu entries
        AddMenuItem("Draught game", new Point(10, 0), 30);

        // Add blocks
        Rectangle rect = new Rectangle(620, 50, 200, 300);
        Color color = Color.color(0.9f, 0.9f, 0.8f);
        rect.setFill(color);
        root.getChildren().add(rect);

        rect = new Rectangle(620, 360, 200, 260);
        rect.setFill(color);
        root.getChildren().add(rect);

        AddMenuItem("Options:", new Point(625, 52), 14);
        AddMenuItem("Game level:", new Point(625, 70), 15);
        Text basic = AddMenuItem("Basic", new Point(650, 90), 15);
        Text medium = AddMenuItem("Medium", new Point(650, 105), 15);
        Text advances = AddMenuItem("Advanced", new Point(650, 120), 15);

        medium.setFill(Color.BLUE);

        basic.setOnMouseClicked((MouseEvent e) -> {
            AI.searchDeep = 4;
            basic.setFill(Color.BLUE);
            medium.setFill(Color.BLACK);
            advances.setFill(Color.BLACK);
        });

        medium.setOnMouseClicked((MouseEvent e) -> {
            AI.searchDeep = 6;
            basic.setFill(Color.BLACK);
            medium.setFill(Color.BLUE);
            advances.setFill(Color.BLACK);
        });

        advances.setOnMouseClicked((MouseEvent e) -> {
            AI.searchDeep = 9;
            basic.setFill(Color.BLACK);
            medium.setFill(Color.BLACK);
            advances.setFill(Color.BLUE);
        });

        AddMenuItem("Statistics:", new Point(625, 365), 14);
        AddMenuItem("Opponent's pawns count:", new Point(625, 385), 12);
        opCount = AddMenuItem("-", new Point(780, 385), 12);

        AddMenuItem("Opponent's dames count:", new Point(625, 405), 12);
        opDCount = AddMenuItem("-", new Point(780, 405), 12);


        AddMenuItem("My pawns count:", new Point(625, 425), 12);
        myCount = AddMenuItem("-", new Point(780, 425), 12);

        AddMenuItem("My dames count:", new Point(625, 445), 12);
        myDCount = AddMenuItem("-", new Point(780, 445), 12);


        AddMenuItem("Alpha-Beta deep:", new Point(625, 465), 12);

        sDeep = AddMenuItem("-", new Point(780, 465), 12);


        AddMenuItem("Last move alpha value:", new Point(625, 485), 12);

        sBeta = AddMenuItem("-", new Point(780, 485), 12);

        AddMenuItem("Your/computer move", new Point(300, 640), 30);
    }
}
