/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draughtsgame.ui;

import javafx.animation.AnimationTimer;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

import java.awt.*;
import java.util.Vector;


class Board extends Canvas {
    static final int boardFieldSize = 70;

    private final Vector<Pawn> pawns;

    GraphicsContext board_gc;
    GraphicsContext gc;
    private UserInterface uiInterface;
    private Group root;


    Board(UserInterface uiInterface) {
        super(600, 600);
        this.setTranslateX(50);
        this.setTranslateY(50);

        this.uiInterface = uiInterface;

        pawns = new Vector<>();
        this.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent t) {
                        // Match which board cell were clicked
                        Point boardPoint = new Point((int) Math.floor(t.getX() / boardFieldSize), (int) Math.floor(t.getY() / boardFieldSize));

                        for (Pawn pawn : pawns) {
                            if (pawn.point.equals(boardPoint)) {
                                uiInterface.clickManager.handle(boardPoint);
                                return;
                            }
                        }
                        uiInterface.clickManager.ClickedEmptySpace(boardPoint);
                    }
                });

        this.gc = getGraphicsContext2D();
        DrawBoard();

        final LongProperty lastUpdateTime = new SimpleLongProperty(0);
        final AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                if (root == null)
                    return;
                if (lastUpdateTime.get() > 0) {
                    synchronized (pawns) {
                        for (Pawn pawn : pawns) {
                            if (!pawn.display) {
                                for (Shape shape : pawn.shapes)
                                    root.getChildren().add(shape);
                                pawn.display = true;
                            }
                        }
                    }
                }

                lastUpdateTime.set(timestamp);
            }

        };

        timer.start();
    }

    void MoveDraught(Point from, Point to) {
        synchronized (pawns) {
            for (Pawn pawn : pawns) {
                if (pawn.point.equals(from)) {
                    pawn.SetPosition(to);
                    break;
                }
            }
        }
    }

    void RemoveDraught(Point position) {
        synchronized (pawns) {
            for (Pawn pawn : pawns) {
                if (pawn.point.equals(position)) {
                    for (Shape shape : pawn.shapes)
                        root.getChildren().remove(shape);
                    pawns.remove(pawn);
                    break;
                }
            }
        }
    }

    void AddDraught(Point point, boolean white) {
        synchronized (pawns) {
            pawns.addElement(new Pawn(point, white, root));
        }
    }

    void AddDame(Point point, boolean white) {
        synchronized (pawns) {
            pawns.addElement(new Dame(point, white, root));
        }
    }

    private void DrawBoard() {
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {

                if ((x + y) % 2 == 0)
                    gc.setFill(Color.WHITE);
                else
                    gc.setFill(Color.BLACK);

                gc.fillRect(boardFieldSize * x, boardFieldSize * y,
                        boardFieldSize, boardFieldSize);
            }
        }

    }

    void setPane(Group root) {
        this.root = root;
    }
}
