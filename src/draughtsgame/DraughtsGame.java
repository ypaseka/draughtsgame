/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draughtsgame;

import draughtsgame.ui.UserInterface;

public class DraughtsGame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Game start");
        Game game = new Game();
        UserInterface.launchUI(args);
    }

}
