package draughtsgame;

import ai.AI;
import static draughtsgame.LogicalBoard.GetBoardIndex;
import java.awt.Point;
import ai.GameState;
import java.util.Vector;

/**
 * The class responsible for logic in the game. It will call the artificial
 * intelligence classes to get decision of computer moves. It also checks all
 * user game moves and accepts or not.
 *
 * The class will prepare events for UI class.
 */
public class GameLogic {

    private final AI ai;
    private final Game game;
    private final LogicalBoard board;

    LogicalBoard.PointFeedback uiFeedback;

    public GameLogic(Game game) {
        this.game = game;
        this.board = new LogicalBoard();
        this.ai = new AI(this.board);
        this.uiFeedback = (Point point) -> game.RemoveDraughtsman(point, false);
    }

    public void RequestInitialState() {
        Vector<Draught> pawns = board.GetAllPawns();
        for (Draught draught : pawns) {
            if (draught.dame) {
                game.CreateDame(draught.point, draught.white);
            } else {
                game.CreateDraughtsman(draught.point, draught.white);
            }
        }
    }

    // Point which have to continue kicking
    Point duringDoubleKicking = null;

    /**
     * Get move suggest of black draughtmans from AI and make the move
     */
    public void ComputerTurn() {
        if (board.IsGameEnd() != 0) {
            game.GameFinished(board.IsGameEnd());
            return;
        }
        
        Move move = ai.GetMove(duringDoubleKicking);
        if (duringDoubleKicking != null) {
            // We are in the middle of double kick, so only its second part can occur
            if (!move.from.equals(duringDoubleKicking)) {
                System.out.println("AI haven't finished double kick");
            } else {
                duringDoubleKicking = null;
            }
        }

        LogicalBoard.MoveReturn moveReturn = board.Move(move, uiFeedback);
        if (moveReturn != LogicalBoard.MoveReturn.FAILED) {
            ai.InformAboutMove(move);

            // Move in the UI
            game.MoveDraughtsman(move.from, move.to, true);
            if (moveReturn == LogicalBoard.MoveReturn.WAITING) {
                // Double kick is here
                System.out.println("Double kick. Waiting for decision");
                board.Print();
                duringDoubleKicking = move.to;
                ComputerTurn();
            } else if (move.to.getY() == 7) {
                game.RemoveDraughtsman(move.to, true);
                game.CreateDame(move.to, false);
            }

        } else {
            System.out.println("AI suggested invalid move - fix it");
        }
        
        
        if (board.IsGameEnd() != 0) {
            game.GameFinished(board.IsGameEnd());
            return;
        }
    }

    void HandleMoveRequest(Point draughtsmanPosition, Point draughtsmanAim) {        
        boolean rightMove = true;
        // For now player can play only white
        if (board.checkPosition(draughtsmanPosition) == LogicalBoard.fieldState.WHITE_DRAUGHT
                || board.checkPosition(draughtsmanPosition) == LogicalBoard.fieldState.WHITE_DAME) {
            // There is our draught - we can move

            if (board.checkPosition(draughtsmanAim) != LogicalBoard.fieldState.EMPTY) {
                // We can't move here, because something stay here
                rightMove = false;
            } else {
                board.GetWhiteBlackElementsCount();
                // We should check further conditions.
                Move move = new Move(draughtsmanPosition, draughtsmanAim);

                Vector<Point> temp = board.ListObligatoryKick(LogicalBoard.fieldPlayer.WHITE);

                // There is any obligatory kick
                if (!temp.isEmpty()) {
                    // Get kicks of the selected figure
                    Vector<Point> figuresKick = board.ObligatoryKick(draughtsmanPosition);
                    if (!figuresKick.contains(draughtsmanAim)) {
                        // Bad move. Something have to be kicked
                        System.out.println("Bad move. Something have to be kicked");
                        rightMove = false;
                    } else if (duringDoubleKicking != null) {
                        if (!draughtsmanPosition.equals(duringDoubleKicking)) {
                            rightMove = false;
                        } else {
                            duringDoubleKicking = null;
                        }
                    }
                }

                // Move on board
                if (rightMove) {
                    LogicalBoard.MoveReturn moveReturn = board.Move(move, uiFeedback);
                    if (moveReturn == LogicalBoard.MoveReturn.FAILED) {
                        return;
                    }

                    ai.InformAboutMove(move);

                    // Move in the UI
                    game.MoveDraughtsman(draughtsmanPosition, draughtsmanAim, false);
                    if (moveReturn == LogicalBoard.MoveReturn.WAITING) {
                        // Double kick is here
                        System.out.println("Double kick. Waiting for decision");
                        duringDoubleKicking = draughtsmanAim;
                        return;
                    }

                    if (draughtsmanAim.getY() == 0) {
                        game.RemoveDraughtsman(draughtsmanAim, false);
                        game.CreateDame(draughtsmanAim, true);
                    }

                    ComputerTurn();
                }
            }
        }
    }

    public int GetDraughtmansCount(LogicalBoard.fieldState fieldType) {
        return board.GetDraughtmansCount(fieldType);
    }


}
