/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draughtsgame;

import draughtsgame.ui.Menu;
import draughtsgame.ui.UserInterface;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manager class. It is responsible for creating a logic and ui class instance.
 * It is used as Controler object as in MVC pattern.
 * Logic and UI communicates via the "Controler" Game object.
 */
public class Game {
    public static Game instance;

    private final GameLogic logic;
    public UserInterface ui;

    public Game() {
        System.out.println("Game started");

        logic = new GameLogic(this);

        Game.instance = this;

        // testUI();
    }


    // Controler class methods

    public void requestDrawing() {
        if (ui != null)
            ui.changeInUI();
    }

    /**
     * Called from UI when it is created and ready to display.
     * Here start board state is set and displayed.
     */
    public void UIInited() {
        logic.RequestInitialState();
    }

    /**
     * Send from UI to indicate move request.
     * The move may be performed from the logic and the UI will be informed by the MoveDraughtsman()
     *
     * @param draughtsmanPosition
     * @param draughtsmanAim
     * @see MoveDraughtsman
     */
    public void MoveRequest(Point draughtsmanPosition, Point draughtsmanAim) {
        logic.HandleMoveRequest(draughtsmanPosition, draughtsmanAim);
    }

    /**
     * Called from Logic to create dame in the UI.
     *
     * @param draughtsmanPosition The position of draughtsman dame.
     */
    void CreateDame(Point draughtsmanPosition, boolean white) {
        ui.CreateDame(draughtsmanPosition, white);
    }

    /**
     * Called from Logic to create new draughtsman in the UI.
     *
     * @param draughtsmanPosition The position of draughtsman to remove.
     */
    void CreateDraughtsman(Point draughtsmanPosition, boolean white) {
        ui.CreateDraughtsman(draughtsmanPosition, white);
    }

    /**
     * Called from Logic to remove the draughtsman in the UI.
     *
     * @param draughtsmanPosition The position of draughtsman to remove.
     */
    void RemoveDraughtsman(Point draughtsmanPosition, boolean opponent) {
        ui.RemoveDraughtsman(draughtsmanPosition);
    }

    void GameFinished(int whoWon) {
        if (whoWon == -1)
            Menu.menu.wonPlane(true);
        else
            Menu.menu.wonPlane(false);
    }

    private Thread t;

    /**
     * Called from Logic to move the draughtsman in the UI.
     *
     * @param draughtsmanPosition
     * @param draughtsmanAim
     */
    void MoveDraughtsman(Point draughtsmanPosition, Point draughtsmanAim, boolean opponent) {
        try {
            if (t != null)
                t.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
        t = new Thread(() -> {
            if (opponent) {
                try {
                    Thread.sleep(1111);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ui.MoveDraughtsman(draughtsmanPosition, draughtsmanAim);

        });
        t.start();
    }

    /**
     * Method for testing UI interaction
     */
    private void testUI() {
        Thread test_thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);

                    Point draughtsmanPosition = new Point(4, 4);
                    CreateDraughtsman(draughtsmanPosition, true);

                    Thread.sleep(1000);
                    draughtsmanPosition.x++;
                    draughtsmanPosition.y++;
                    CreateDame(draughtsmanPosition, false);

                    Thread.sleep(1000);
                    // MoveDraughtsman(draughtsmanPosition, new Point(draughtsmanPosition.x+2, draughtsmanPosition.y+2));

                } catch (InterruptedException ex) {
                    Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        test_thread.start();
    }

    public int GetDraughtmansCount(LogicalBoard.fieldState fieldType) {
        return logic.GetDraughtmansCount(fieldType);
    }
}
