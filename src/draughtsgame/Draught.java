/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package draughtsgame;

import java.awt.*;

/**
 * The class represents Draught
 */
public class Draught {
    public final Point point;
    public final boolean white;
    final boolean dame;

    public Draught(Point point, int state) {
        this.point = point;

        if (state == LogicalBoard.fieldState.WHITE_DRAUGHT.ordinal()) {
            this.white = true;
            this.dame = false;
        } else if (state == LogicalBoard.fieldState.WHITE_DAME.ordinal()) {
            this.white = true;
            this.dame = false;
        } else if (state == LogicalBoard.fieldState.BLACK_DRAUGHT.ordinal()) {
            this.white = false;
            this.dame = false;
        } else {
            this.white = false;
            this.dame = false;
        }
    }
}
