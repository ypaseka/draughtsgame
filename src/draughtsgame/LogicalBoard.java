package draughtsgame;

import java.awt.*;
import java.util.Arrays;
import java.util.Vector;

/**
 * This class is to manage board. It gives easy to handle interface over table.
 */
public class LogicalBoard {

    public int[] integerBoard;

    public enum fieldState {
        EMPTY, WHITE_DRAUGHT, WHITE_DAME, BLACK_DRAUGHT, BLACK_DAME
    }

    public enum fieldPlayer {
        BLACK, WHITE
    }

    private String North_West = "North_West";
    private String North_East = "North_East";
    private String South_West = "South_West";
    private String South_East = "South_East";
    static final int DRAUGHTS_PER_ROW = 4;

    public LogicalBoard() {
        integerBoard = new int[32];
        ResetBoard();
    }

    @Override
    public LogicalBoard clone() {
        LogicalBoard newBoard = new LogicalBoard();
        newBoard.integerBoard = integerBoard.clone();

        return newBoard;
    }

    //needed for AI
    public int[] getIntegerBoard() {
        return integerBoard;
    }

    private void ResetBoard() {
        Arrays.fill(integerBoard, fieldState.EMPTY.ordinal());
        for (int x = 0; x < DRAUGHTS_PER_ROW * 3; x++) {
            integerBoard[x] = fieldState.BLACK_DRAUGHT.ordinal();
        }
        for (int x = 32 - DRAUGHTS_PER_ROW * 3; x < 32; x++) {
            integerBoard[x] = fieldState.WHITE_DRAUGHT.ordinal();
        }
    }

    // For debug only
    public void Print() {
        System.out.println();

        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 4; x++) {
                if (y % 2 == 0) {
                    System.out.print("-");
                }

                System.out.print(integerBoard[x + 4 * y]);

                if (y % 2 == 1) {
                    System.out.print("-");
                }
            }

            System.out.println();
        }
    }

    /**
     * Method compare two field and return true, while there are opponents
     *
     * @param pos1
     * @param pos2
     * @return
     */
    private boolean areOpponents(Point pos1, Point pos2) {
        fieldState fs1 = checkPosition(pos1);
        fieldState fs2 = checkPosition(pos2);
        if (fs1 == fieldState.EMPTY || fs2 == fieldState.EMPTY) {
            return false;
        }
        if (fs1 == fs2) {
            return false;
        }
        if (fs1 == fieldState.WHITE_DRAUGHT && fs2 == fieldState.WHITE_DAME
                || fs2 == fieldState.WHITE_DRAUGHT && fs1 == fieldState.WHITE_DAME) {
            return false;
        }
        return (fs1 != fieldState.BLACK_DRAUGHT || fs2 != fieldState.BLACK_DAME)
                && (fs2 != fieldState.BLACK_DRAUGHT || fs1 != fieldState.BLACK_DAME);
    }

    private boolean areFriends(Point pos1, Point pos2) {
        fieldState fs1 = checkPosition(pos1);
        fieldState fs2 = checkPosition(pos2);
        if (fs1 == fieldState.EMPTY || fs2 == fieldState.EMPTY) {
            return false;
        }
        if (fs1 == fs2) {
            return true;
        }
        if (fs1 == fieldState.WHITE_DRAUGHT && fs2 == fieldState.WHITE_DAME
                || fs2 == fieldState.WHITE_DRAUGHT && fs1 == fieldState.WHITE_DAME) {
            return true;
        }
        return fs1 == fieldState.BLACK_DRAUGHT && fs2 == fieldState.BLACK_DAME
                || fs2 == fieldState.BLACK_DRAUGHT && fs1 == fieldState.BLACK_DAME;

    }

    /**
     * Check status of field from integerBoard[32] table.
     *
     * @param position
     * @return
     */
    fieldState checkPosition(Point position) {
        position = DenormalizePoint(position);
        int positionIndex = position.x + position.y * 4;

        return fieldState.values()[integerBoard[positionIndex]];
    }

    /**
     * From 8x8 to 4x8
     */
    private static Point DenormalizePoint(Point pt) {
        Point ret = new Point(pt.x, pt.y);
        ret.x = ret.x / 2;
        return ret;
    }

    /**
     * Get index of 8x4 fields from 8x4 board
     */
    public static int GetBoardIndex(Point pt) {
        pt = DenormalizePoint(pt);
        return pt.x + pt.y * 4;
    }

    private static Point getExtendedPoint(int index) {
        int y = index / 4;
        int x = index % 4;
        Point ret = new Point();
        if (y % 2 == 0) {
            ret.x = x * 2 + 1;
        } else {
            ret.x = x * 2;
        }
        ret.y = y;
        return ret;
    }

    /**
     * Gets a list of points which are non empty.
     *
     * @return The vector of points on 8x8 board
     */
    Vector<Draught> GetAllPawns() {
        Vector<Draught> pawns = new Vector<>();
        int index = 0;
        for (int i = 0; i < 32; i++) {
            int cfieldState = integerBoard[i];
            if (cfieldState != fieldState.EMPTY.ordinal()) {
                Draught d = new Draught(getExtendedPoint(index), cfieldState);
                pawns.addElement(d);
            }
            index++;
        }
        return pawns;
    }

    /**
     * Gets a list of points which are non empty.
     *
     * @return The vector of points on 8x8 board
     */
    public Vector<Point> GetNonEmptyFields() {
        Vector<Point> points = new Vector<>();
        int index = 0;
        for (int i = 0; i < 32; i++) {
            int cfieldState = integerBoard[i];
            if (cfieldState != fieldState.EMPTY.ordinal()) {
                points.addElement(getExtendedPoint(index));
            }
            index++;
        }
        return points;
    }

    /**
     * Gets list of points which are kick end points
     *
     * @param figurePoint Point where the figure stays
     * @return Vector of points
     */
    public Vector<Point> ObligatoryKick(Point figurePoint) {
        Vector<Point> pointsTo = new Vector<>();
        fieldState fsFr = checkPosition(figurePoint);

        // implementation for Draughts
        if (isDraught(fsFr)) {
            pointsTo.add(new Point((int) figurePoint.getX() + 2, (int) figurePoint.getY() + 2));
            pointsTo.add(new Point((int) figurePoint.getX() - 2, (int) figurePoint.getY() + 2));
            pointsTo.add(new Point((int) figurePoint.getX() + 2, (int) figurePoint.getY() - 2));
            pointsTo.add(new Point((int) figurePoint.getX() - 2, (int) figurePoint.getY() - 2));

            for (int i = 3; i >= 0; i--) {
                Move move = new Move(figurePoint, pointsTo.elementAt(i));
                if (CanMove(move) == null) {
                    pointsTo.removeElementAt(i);
                }
            }
        }

        // implementation for Dames
        if (isDame(fsFr)) {

            Vector<Point> directions = new Vector<>();
            directions.add(new Point(-1, -1));
            directions.add(new Point(1, 1));
            directions.add(new Point(-1, 1));
            directions.add(new Point(1, -1));

            for (Point deltaPoint : directions) {
                Point next = (Point) figurePoint.clone();
                Point temp = new Point();
                boolean checkForEmpty = false;
                do {
                    next.x += deltaPoint.x;
                    next.y += deltaPoint.y;

                    if (!(next.x > -1 && next.x < 8 && next.y > -1 && next.y < 8)) {
                        break;
                    }

                    if (areFriends(temp, next)) {
                        break;
                    }

                    if (checkForEmpty) {
                        if (!areOpponents(figurePoint, next)) {
                            pointsTo.add((Point) next.clone());
                        } else {
                            break;
                        }
                    }

                    if (areOpponents(figurePoint, next)) {
                        checkForEmpty = true;
                    }
                    temp = (Point) next.clone();
                } while (true);
            }
        }

        return pointsTo;
    }

    /**
     * Gets the player which the given figure is
     */
    private fieldPlayer GetFieldPlayer(int fieldIndex) {
        int cfieldState = integerBoard[fieldIndex];
        if (cfieldState < 3) {
            return fieldPlayer.WHITE;
        } else {
            return fieldPlayer.BLACK;
        }
    }

    /**
     * List of all points, which have to kick
     *
     * @param fieldType, example: ListObligatoryKick(WHITE_DRAUGHT)
     * @return The vector of points
     */
    Vector<Point> ListObligatoryKick(fieldPlayer fieldType) {
        Vector<Point> pointsToKick = new Vector<>();
        for (int i = 0; i < 32; i++) {
            if (GetFieldPlayer(i) == fieldType) {
                pointsToKick.addAll(ObligatoryKick(getExtendedPoint(i)));
            }
        }
        return pointsToKick;
    }

    private boolean IsObligatoryKick(fieldPlayer fieldType) {
        Vector<Point> pointsToKick = new Vector<>();
        for (int i = 0; i < 32; i++) {
            if (GetFieldPlayer(i) == fieldType) {
                if (!(ObligatoryKick(getExtendedPoint(i)).isEmpty())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Gets a list of all possible moves of specified figures
     *
     * @param fieldType Type of field to get Moves.
     * @return The vector of points of pawns position
     */
    public Vector<Move> GetPossibleMoves(fieldPlayer fieldType) {
        Vector<Move> moves = new Vector<>();

        Vector<Point> kicks = ListObligatoryKick(fieldType);
        boolean isAnyKick = IsObligatoryKick(fieldType);

        for (int i = 0; i < 32; i++) {
            int cfieldState = integerBoard[i];
            if (GetFieldPlayer(i) == fieldType) {

                Point figurePoint = getExtendedPoint(i);

                Vector<Point> pointsTo = null;
                if (isAnyKick) {
                    pointsTo = ObligatoryKick(figurePoint);
                } else {
                    pointsTo = new Vector<>();

                    if (isDraught(cfieldState)) {
                        if (fieldType == fieldPlayer.BLACK) {
                            pointsTo.add(new Point((int) figurePoint.getX() + 1, (int) figurePoint.getY() + 1));
                            pointsTo.add(new Point((int) figurePoint.getX() - 1, (int) figurePoint.getY() + 1));
                        } else {
                            pointsTo.add(new Point((int) figurePoint.getX() + 1, (int) figurePoint.getY() - 1));
                            pointsTo.add(new Point((int) figurePoint.getX() - 1, (int) figurePoint.getY() - 1));
                        }
                    } else {
                        Vector<Point> directions = new Vector<>();
                        directions.add(new Point(-1, -1));
                        directions.add(new Point(1, 1));
                        directions.add(new Point(-1, 1));
                        directions.add(new Point(1, -1));

                        for (Point deltaPoint : directions) {
                            Point next = (Point) figurePoint.clone();
                            Point temp = new Point();
                            do {
                                next.x += deltaPoint.x;
                                next.y += deltaPoint.y;

                                if (!(next.x > -1 && next.x < 8 && next.y > -1 && next.y < 8)) {
                                    break;
                                }

                                pointsTo.add((Point) next.clone());
                            } while (true);
                        }
                    }
                }

                for (Point anyPoint : pointsTo) {
                    Move move = new Move(figurePoint, anyPoint);
                    try {
                        if (CanMove(move) != null) {
                            moves.addElement(move);
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
            }
        }
        return moves;
    }

    private String checkDirection(Point from, Point to) {
        int deltaY = to.y - from.y;
        int deltaX = to.x - from.x;
        int tgAlfa = deltaY / deltaX;
        String dir = null;
        //tgAlfa is correct! Because the Board is inverted.
        if (tgAlfa == -1 && deltaX > 0 && deltaY < 0) {
            dir = North_East;
        }
        if (tgAlfa == -1 && deltaX < 0 && deltaY > 0) {
            dir = South_West;
        }
        if (tgAlfa == 1 && deltaX > 0 && deltaY > 0) {
            dir = South_East;
        }
        if (tgAlfa == 1 && deltaX < 0 && deltaY < 0) {
            dir = North_West;
        }
        return dir;
    }

    private void changeState(Point pt, fieldState fs) {
        int position = GetBoardIndex(pt);
        integerBoard[position] = fs.ordinal();
    }

    private boolean isDraught(int fs) {
        return fs % 2 == 1; // Because Draught is 1 for white and 3 for black
    }

    private boolean isDraught(fieldState fs) {
        return fs == fieldState.BLACK_DRAUGHT || fs == fieldState.WHITE_DRAUGHT;
    }

    private boolean isDame(fieldState fs) {
        return fs == fieldState.BLACK_DAME || fs == fieldState.WHITE_DAME;
    }

    private Point calcMiddlePoint(Point p1, Point p2) {
        Point middle = new Point();
        middle.x = (p1.x + p2.x) / 2;
        middle.y = (p1.y + p2.y) / 2;
        return middle;
    }

    private boolean CanWhiteMoveForward(Point from, Point to) {

        return checkPosition(from) == fieldState.WHITE_DRAUGHT && (checkDirection(from, to).equals(North_East)
                || checkDirection(from, to).equals(North_West));
    }

    private boolean CanBlackMoveForward(Point from, Point to) {

        return checkPosition(from) == fieldState.BLACK_DRAUGHT && (checkDirection(from, to).equals(South_East)
                || checkDirection(from, to).equals(South_West));
    }

    private void SwapFields(Point f1, Point f2) {
        fieldState f1State = checkPosition(f1);
        fieldState f2State = checkPosition(f2);

        changeState(f2, f1State);
        changeState(f1, f2State);
    }

    public interface PointFeedback {
        public void feedback(Point point);
    }

    /**
     * Tries to move draught between specified fields
     *
     * @param move
     * @param removeFeedback
     * @return failed, succeeded, waitingForFinalizing when the move is double
     * kick
     */
    public enum MoveReturn {
        FAILED,
        SUCCEEDED,
        WAITING
    }

    public MoveReturn Move(Move move, PointFeedback removeFeedback) {
        Vector<Point> killedDraughts = CanMove(move);
        if (killedDraughts != null) {
            SwapFields(move.from, move.to);

            for (Point point : killedDraughts) {
                changeState(point, fieldState.EMPTY);

                if (removeFeedback != null) {
                    removeFeedback.feedback(point);
                }
            }

            // Check for double kick
            if (killedDraughts.size() > 0) {
                Vector<Point> figuresKick = ObligatoryKick(move.to);
                if (!figuresKick.isEmpty()) {
                    return MoveReturn.WAITING;
                }
            }

            // Check whether pawn shouldn't be changed to dame
            if ((move.to.getY() == 7 && checkPosition(move.to) == fieldState.BLACK_DRAUGHT)
                    || (move.to.getY() == 0 && checkPosition(move.to) == fieldState.WHITE_DRAUGHT)) {
                Promote(move.to);
            }
            return MoveReturn.SUCCEEDED;
        }

        return MoveReturn.FAILED;
    }

    /**
     * Promotes pawn to dame
     */
    private void Promote(Point position) {
        int positionIndex = GetBoardIndex(position);

        if (integerBoard[positionIndex] == 1 || integerBoard[positionIndex] == 3) {
            integerBoard[positionIndex]++;
        }
    }

    int IsGameEnd() {
        if (GetWhiteBlackElementsCount().firstElement().equals(0))
            return 1;
        else if (GetWhiteBlackElementsCount().lastElement().equals(0))
            return -1;
        else
            return 0;
    }

    /**
     * Checks whether draught can move and returns vector with killed opponent
     * points
     *
     * @param move
     * @return vector with killed opponent points or null if move is not
     * possible
     */
    public Vector<Point> CanMove(Move move) {

        boolean moveSucceeded = false;
        Vector<Point> removedPoints = new Vector<>();

        if (!(move.to.x > -1 && move.to.x < 8 && move.to.y > -1 && move.to.y < 8)) {
            return null;
        }


        fieldState fsFr = checkPosition(move.from);
        fieldState fsTo = checkPosition(move.to);

        if (fsFr != fieldState.EMPTY && fsTo != fieldState.EMPTY) {
            return null;
        }


        // Kick and Move for DAME in all direction
        if (isDame(fsFr)) {

            int deltaX = 0;
            int deltaY = 0;

            int numopp = 0;
            Point next = new Point();
            Point opponentPoint = null;
            Point temp = (Point) move.from.clone();

            deltaX = move.to.x - move.from.x;
            deltaY = move.to.y - move.from.y;
            if (deltaY == 0 || deltaX == 0) {
                return null;
            }

            deltaX /= Math.abs(deltaX);
            deltaY /= Math.abs(deltaY);

            do {
                next.x = temp.x + deltaX;
                next.y = temp.y + deltaY;

                if (areFriends(temp, next)) {
                    return null;
                }

                if (areOpponents(move.from, next)) {
                    numopp++;
                    if (numopp < 2) {
                        opponentPoint = (Point) next.clone();
                    }
                }
                temp = (Point) next.clone();
            } while (!move.to.equals(next) && numopp < 2);

            if (move.to.equals(next)) {
                if (opponentPoint != null) {
                    removedPoints.add(opponentPoint);
                }
                moveSucceeded = true;
            }
        } else if (isDraught(fsFr)) {
            // move near field in all direction
            int vector = Math.abs(move.from.x - move.to.x);
            int vector2 = Math.abs(move.from.y - move.to.y);

            if (vector == 1 && vector2 == 1) {
                if (CanWhiteMoveForward(move.from, move.to) || CanBlackMoveForward(move.from, move.to)) {
                    moveSucceeded = true;
                }
            } // kick near oponent(jump over him) in all direction
            else if (vector == 2 && vector2 == 2) {
                Point middle = calcMiddlePoint(move.from, move.to);
                if (areOpponents(move.from, middle)) {
                    // Kicking opponents pawn
                    removedPoints.add(middle);
                    moveSucceeded = true;
                }
            }
        }
        return moveSucceeded ? removedPoints : null;
    }

    // Functions for estimating game state

    /**
     * Gets a list of points which are non empty.
     *
     * @return The vector of points on 8x8 board
     */
    public int GetDraughtmansCount(fieldState fieldType) {
        int count = 0;
        for (int cfieldState : integerBoard) {
            if (cfieldState == fieldType.ordinal()) {
                count++;
            }
        }
        return count;
    }

    Vector<Integer> GetWhiteBlackElementsCount() {
        Vector<Integer> count = new Vector<Integer>();
        int countWhite = 0;
        int countBlack = 0;
        for (int i = 0; i < 32; i++) {
            if (checkPosition(getExtendedPoint(i)) == fieldState.WHITE_DAME || checkPosition(getExtendedPoint(i)) == fieldState.WHITE_DRAUGHT) {
                countWhite++;
            } else if (checkPosition(getExtendedPoint(i)) == fieldState.BLACK_DAME || checkPosition(getExtendedPoint(i)) == fieldState.BLACK_DRAUGHT) {
                countBlack++;
            }
        }
        // System.out.println("Count WHITE: " + countWhite + " Count BLACK: " + countBlack);
        count.addElement(countWhite);
        count.addElement(countBlack);
        return count;
    }

    //for test
    public static void main(String[] args) {
        LogicalBoard lb = new LogicalBoard();
        lb.Print();
        lb.changeState(new Point(4, 5), fieldState.WHITE_DAME);
        lb.changeState(new Point(5, 6), fieldState.EMPTY);

        lb.Move(new Move(new Point(1, 2), new Point(2, 3)), null);
        lb.Print();
        lb.Move(new Move(new Point(4, 5), new Point(1, 2)), null);
        lb.Print();
        lb.Move(new Move(new Point(1, 2), new Point(4, 5)), null);
        lb.Print();
        lb.Move(new Move(new Point(4, 5), new Point(6, 3)), null);
        lb.Print();

        // Test of normalization and denormalization
        for (int i = 0; i < 32; i++) {
            Point point = getExtendedPoint(i);
            System.out.println("Index:" + i + " " + "ExtendedPoint 8x8:" + point + " ");
            int indexPoint = GetBoardIndex(point);
            System.out.println("indexPoint:" + indexPoint + " " + "ExtendedPoint 8x8:" + point + " " + "\n");
            if (i != indexPoint) {
                System.out.println(i + " != " + indexPoint);
            }
        }
    }

}
