package ai;

import draughtsgame.LogicalBoard;
import draughtsgame.Move;

import java.awt.*;
import java.util.ArrayList;
import java.util.Vector;

public class AI {
    private ZobristHashing zobristHashing;
    private AlphaBeta alphaBeta;
    private GameState currentState;
    static public int searchDeep = 6;
    static public double lastBeta = 0;

    static final int HASH_ARRAY_LEN = 100000000; // Length of the states hash array

    static public GameState[] gameStates = new GameState[HASH_ARRAY_LEN];

    public AI(LogicalBoard logicalBoard) {
        ZobristHashing.initialize();

        // Set root state
        currentState = new GameState();
        currentState.board = logicalBoard.clone();
        currentState.CalculateHashValue();

        for (int i = 0; i < HASH_ARRAY_LEN; i++)
            gameStates[i] = null;

        alphaBeta = new AlphaBeta();
    }

    /**
     * Updates board with new move information
     *
     * @param move The move of player
     */
    public void InformAboutMove(Move move) {
        // Calculate new state based on the move parameter
        currentState = currentState.GameStateFromMove(move, true);

        System.out.println("Informed: " + move.from + " " + move.to);
        currentState.board.Print();
    }


    /**
     * Makes "best" move for given board state.
     * Makes a move after the players move.
     *
     * @param figurePoint A specified figure poing if the move have to be made by it.
     *                    Used in double kicking
     */
    public Move GetMove(Point figurePoint) {
        Move bestMove = null;
        double maxBeta = 0.0;
        GameState bestMoveState = null;
        ArrayList<GameState> children = new ArrayList<GameState>();

        // Get all possible moves of black pawns and run alpha-beta on each of the resultants states
        Vector<Move> possibleMoves = null;

        if (figurePoint == null) {
            possibleMoves = currentState.board.GetPossibleMoves(LogicalBoard.fieldPlayer.BLACK);
        } else {
            // Get only kicks from given figure
            Vector<Point> figuresKick = currentState.board.ObligatoryKick(figurePoint);
            possibleMoves = new Vector<>();
            for (Point toPoint : figuresKick)
                possibleMoves.add(new Move(figurePoint, toPoint));
        }

        for (Move move : possibleMoves) {
            GameState state = currentState.GameStateFromMove(move, true);

            double beta = 0.0;
            if (state.score >= 0)
                beta = state.score;
            else
                beta = alphaBeta.run(state);

            state.score = beta;

            // Choose better decision than the previous
            if (beta > maxBeta) {
                bestMove = move;
                bestMoveState = state;
                maxBeta = beta;
            }
        }

        lastBeta = maxBeta;

        System.out.println("AI Board: ");
        if (bestMove == null) {
            System.out.println("################## bestMove == null #####################");
            System.out.println("FigPt: " + figurePoint);
            System.out.println("Moves: " + possibleMoves);
        }

        return bestMove;
    }
}
