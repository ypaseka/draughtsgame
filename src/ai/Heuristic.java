package ai;

import draughtsgame.LogicalBoard;

/**
 * Heuristic class manages state estimation
 */
class Heuristic {
    private final int WIN = Integer.MAX_VALUE;
    private final int LOSE = Integer.MIN_VALUE;

    double estymate(LogicalBoard board) {
        int playerDraughtmans = board.GetDraughtmansCount(LogicalBoard.fieldState.BLACK_DRAUGHT) + 2 * board.GetDraughtmansCount(LogicalBoard.fieldState.BLACK_DAME);
        int opponentsDraughtmans = board.GetDraughtmansCount(LogicalBoard.fieldState.WHITE_DRAUGHT) + 2 * board.GetDraughtmansCount(LogicalBoard.fieldState.WHITE_DAME);

        return (double) playerDraughtmans / opponentsDraughtmans;
    }
}
