package ai;

import draughtsgame.LogicalBoard;
import draughtsgame.Move;

import java.util.ArrayList;
import java.util.Vector;

public class GameState {
    private int hashValue;
    double score;
    boolean isTemporary = false; // This is true if the state is in the middle of double kick
    public LogicalBoard board;

    /*
       best move
       Type of node: Exact, UpperBound, LowerBound
     */

    public GameState() {
        score = -1.0;
    }

    /**
     * Sets hash value.
     * The value is modulo HASH_ARRAY_LEN.
     */
    private void SetHashValue(long hashValue) {
        this.hashValue = (int) (hashValue % AI.HASH_ARRAY_LEN);
    }

    /**
     * Calculate hash for the state.
     */
    void CalculateHashValue() {
        SetHashValue(ZobristHashing.GetHashValue(board));
    }

    /**
     * Calculate hash value for the state.
     *
     * @param move        The move to get this state from previous
     * @param currentHash The hash of parent state
     * @param board       The board of the parent
     */
    private void CalculateHashValue(Move move, long currentHash, LogicalBoard parentBoard) {
        // Calculate state hash value after the considered move
        long hashDifference = ZobristHashing.CalculateHashDifference(move, parentBoard, board);

        long moveHash = currentHash + hashDifference;
        if (moveHash < 0)
            moveHash += Long.MAX_VALUE;

        SetHashValue(moveHash);
    }

    /**
     * Creates game state from previous state and the move vector.
     * If the state is found in hash table, it gets the found state and board is not copied unless forceBoardCopy is false
     *
     * @param move           The move to generate new state
     * @param forceBoardCopy true if the board have to be copied independently from the found hashes. This is to secure the move
     */
    GameState GameStateFromMove(Move move, boolean forceBoardCopy) {
        GameState state = new GameState();

        // Copy independently on the hash
        state.board = (LogicalBoard) board.clone();
        LogicalBoard.MoveReturn moveReturn = state.board.Move(move, null);

        state.CalculateHashValue(move, hashValue, board);

        // Use only score from state if it was considered before
        if (AI.gameStates[state.hashValue] != null) {
            state.score = AI.gameStates[state.hashValue].score;
        }

        // In the result it will copy only score variable from state
        if (moveReturn == LogicalBoard.MoveReturn.WAITING)
            state.isTemporary = true;

        AI.gameStates[state.hashValue] = state;

        return state;
    }

    public GameState GameStateFromMove(Move move) {
        return GameStateFromMove(move, false);
    }

    /**
     * Get all possible children states.
     * The states have the hash value already calculated.
     *
     * @param whitesMove     true if this move is white player
     * @param forceBoardCopy true if the board have to be copied independently from the found hashes. This is to secure the move
     */
    ArrayList<GameState> GetAllNextStates(boolean whitesMove, boolean forceBoardCopy) {
        ArrayList<GameState> children = new ArrayList<>();

        Vector<Move> possibleMoves = board.GetPossibleMoves(whitesMove ? LogicalBoard.fieldPlayer.WHITE : LogicalBoard.fieldPlayer.BLACK);
        for (Move move : possibleMoves) {
            GameState state = GameStateFromMove(move, forceBoardCopy);
            children.add(state);
        }

        return children;
    }
}
