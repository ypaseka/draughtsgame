package ai;

public class AlphaBeta {
    Heuristic heuristic;

    public AlphaBeta() {
        heuristic = new Heuristic();
    }

    double run(GameState gameState) {
        return AlphaBeta(gameState, Double.MIN_VALUE, Double.MAX_VALUE, AI.searchDeep, true);
    }


    /*
        funkcja przeszukuje drzewo i wybiera najwieksze wartości
        dla MAX i najmniejsze dla MIN
        gdy przeszuka na określoną głębokość wywołuje heurystykę
     */
    private double AlphaBeta(GameState gameState, double alpha, double beta, int depthLeft, boolean maxPlayer) {
        double score;

        if (depthLeft == 0) {
            return heuristic.estymate(gameState.board);
        }

        for (GameState state : gameState.GetAllNextStates(maxPlayer, false)) {
            // Change player and decrement deep unless there is double kick
            if (state.score >= 0)
                score = state.score;
            else
                score = AlphaBeta(state, alpha, beta, state.isTemporary ? depthLeft : depthLeft - 1, state.isTemporary == maxPlayer);
            if (maxPlayer) {
                if (score > alpha)
                    alpha = score;
                if (alpha >= beta)
                    break;
            } else {
                if (score < beta)
                    beta = score;
                if (beta <= alpha)
                    break;
            }
        }

        return maxPlayer ? alpha : beta;
    }
}
