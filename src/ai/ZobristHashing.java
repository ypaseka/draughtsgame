package ai;

import draughtsgame.LogicalBoard;
import draughtsgame.Move;

import java.awt.*;
import java.util.Random;
import java.util.Vector;

class ZobristHashing {
    // Range for game states
    static private final long range = (long) Math.pow(2, 62);
    static private Random random;
    static private long[][] hashTable;

    static long GetHashValue(LogicalBoard logicalBoard) {
        // And calculates hash on the game begining
        long hashValue = hashTable[0][logicalBoard.integerBoard[0]];

        for (int i = 1; i < 32; i++) {
            hashValue += hashTable[i][logicalBoard.integerBoard[i]];
        }

        if (hashValue < 0)
            hashValue += Long.MAX_VALUE;

        return hashValue;
    }

    /**
     * Initializes hashTable
     */
    static void initialize() {
        random = new Random();

        hashTable = new long[32][5];

        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 5; j++) {
                hashTable[i][j] = (long) (random.nextDouble() * range);
            }
        }
    }

    /**
     * Gets a hash for given field on the given board
     */
    private static long GetFieldHash(int fieldIndex, LogicalBoard board) {
        return hashTable[fieldIndex][board.integerBoard[fieldIndex]];
    }

    /**
     * Gets difference in a given field between current and new logical booard
     */
    private static long CalculateHashDifference(Point point, LogicalBoard newBoard, LogicalBoard logicalBoard) {
        int pointOnBoard = LogicalBoard.GetBoardIndex(point);
        long hashDifference = GetFieldHash(pointOnBoard, newBoard);
        hashDifference -= GetFieldHash(pointOnBoard, logicalBoard);
        return hashDifference;
    }

    /**
     * Calculate hash difference for a move
     */
    static long CalculateHashDifference(Move move, LogicalBoard parentLogicalBoard, LogicalBoard newBoard) {
        long hashDifference = 0;
        Vector<Point> killedPoints = parentLogicalBoard.CanMove(move);

        if (killedPoints == null)
            return 0;

        // Difference between old board and the new one
        hashDifference += CalculateHashDifference(move.from, newBoard, parentLogicalBoard);
        hashDifference += CalculateHashDifference(move.to, newBoard, parentLogicalBoard);

        for (Point point : killedPoints) {
            hashDifference += CalculateHashDifference(point, newBoard, parentLogicalBoard);
        }

        if (hashDifference < 0)
            hashDifference += Long.MAX_VALUE;

        return hashDifference;
    }
}
